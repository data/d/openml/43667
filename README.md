# OpenML dataset: Medium-Articles

https://www.openml.org/d/43667

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Medium is one of the most famous tools for spreading knowledge about almost any field. It is widely used to published articles on ML, AI, and data science. This dataset is the collection of about 350 articles in such fields. 
Content
The dataset contains articles, their title, number of claps it has received, their links and their reading time.
Acknowledgements
This dataset was scraped from Medium.  I created a Python script to scrap all the required articles using just their tags from Medium. Check out the script here
Inspiration
How to write a good article? How to inform the reader in an interesting way? What sort of title attracts more crowd? How long an article should be?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43667) of an [OpenML dataset](https://www.openml.org/d/43667). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43667/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43667/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43667/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

